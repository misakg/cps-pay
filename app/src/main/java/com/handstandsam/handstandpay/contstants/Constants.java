package com.handstandsam.handstandpay.contstants;

public class Constants {

    //
    //  We include a prepaid Visa debit card with no balance so the app has a card
    //  configured until the user switches to their own card:
    //
    public static  String DEFAULT_SWIPE_DATA = ";6376351033100052=2501101?";
            //";6376351033100052=2501101?";

}
