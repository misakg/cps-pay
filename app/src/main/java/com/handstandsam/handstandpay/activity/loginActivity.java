package com.handstandsam.handstandpay.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.handstandsam.handstandpay.R;
import com.handstandsam.handstandpay.contstants.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class loginActivity  extends Activity {



    private String URL = "https://apps.compassaws.net/api/card/login-cpsPay";

    private EditText Username;
    private EditText Password;
    private Button Login;

    public static final String PREFS_NAME = "StoredData";
    private static final String PREF_USERNAME = "username";
    private static final String PREF_PASSWORD = "password";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Username = (EditText) findViewById(R.id.etUsername);
        Password = (EditText) findViewById(R.id.etPassword);
        Login = (Button) findViewById(R.id.btnLogin);

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String username = Username.getText().toString().trim();
                final String password = Password.getText().toString().trim();

                JSONObject json = new JSONObject();
                try {
                    json.put("email", username);
                    json.put("password", password);
                    json.put("notificationToken","1");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.POST, URL, json, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                JSONObject data = response;
                                try {
                                    JSONObject results = data.getJSONObject("results");
                                    JSONObject result = results.getJSONObject("result");
                                    Constants.DEFAULT_SWIPE_DATA = result.get("personalMsd").toString();

                                    System.out.println("DATA" + Constants.DEFAULT_SWIPE_DATA);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                try {
                                    if ((response.get("success").toString()).equals("0")){
                                        getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
                                                .edit()
                                                .putString(PREF_USERNAME, username)
                                                .putString(PREF_PASSWORD, password)
                                                .commit();
                                        Intent intent = new Intent(loginActivity.this, PayActivity.class);
                                        startActivity(intent);

                                        Toast.makeText(getApplicationContext(),"test" + Constants.DEFAULT_SWIPE_DATA, Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


//                            Toast.makeText(getApplicationContext(), "Dobar"+ data.get("success"), Toast.LENGTH_SHORT).show();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }){

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "Application/x-www-form-urlencodes");
                        return params;
                    }

                };
                requestQueue.add(jsonObjectRequest);
            }
        });
    }

}